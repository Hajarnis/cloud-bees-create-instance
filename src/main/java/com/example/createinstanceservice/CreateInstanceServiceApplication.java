package com.example.createinstanceservice;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@EnableDiscoveryClient
@SpringBootApplication
public class CreateInstanceServiceApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(CreateInstanceServiceApplication.class, args);
	}
}

@RefreshScope
@RestController
class CreateInstanceRestController{
	
	

	@RequestMapping(value="/createInstance", method=RequestMethod.POST)
	public String createInstance(@RequestBody CreateComputeEngine createComputeEngine){
		
		CreateComputeEngine newComputeEngine = new CreateComputeEngine(
				createComputeEngine.getApplicationName(),
				createComputeEngine.getProjectId(),
				createComputeEngine.getZoneName(), 
				createComputeEngine.getSampleInstanceName(), 
				createComputeEngine.getSourceImagePrefix(), 
				createComputeEngine.getSourceImagePath());
		
		return newComputeEngine.createInstance();
	}
}

